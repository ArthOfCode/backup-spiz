import {Component, OnInit} from '@angular/core';
import {remote} from 'electron';
import * as fs from 'fs';
import * as os from 'os';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public rootFile = '';
  public targetFolder = '';

  constructor() {
  }

  ngOnInit() {
    this.rootFile = this.getPathFromLocalStorage();
  }
  private getPathFromLocalStorage() {
    return localStorage.getItem('rootPath');
  }
  private setPathFromLocalStorage(path: string) {
    return localStorage.setItem('rootPath', path);
  }
  getRootFile() {
    remote.dialog.showOpenDialog({
      properties: ['openFile']
    }, (filesPaths: string[]) => {
      this.rootFile = filesPaths[0];
      this.setPathFromLocalStorage(this.rootFile);
    });
  }
  isEnough() {
    return this.targetFolder.length > 0 && this.rootFile.length > 0;
  }
  getTargetDirectory() {
    remote.dialog.showOpenDialog({
      properties: ['openDirectory']
    }, (filesPaths: string[]) => {
      this.targetFolder = filesPaths[0];
    });
  }

  copy() {
    this.createFolder();
  }

  private formatDate(date) {
    const d = new Date(date);
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();
    const year = d.getFullYear();

    if (month.length < 2) {
      month = '0' + month;
    };

    if (day.length < 2) {
      day = '0' + day;
    };

    return [year, month, day].join('-');
  }
  private createFolder() {
    const date = this.formatDate(new Date());
    const path = this.targetFolder + '/' + date;
    let separator = '/';
    if (os.platform() === 'win32') {
      separator = '\\';
    }
    const fileName = this.rootFile.split(separator)[this.rootFile.split(separator).length - 1];
    fs.mkdir(path, undefined, () => {
      fs.copyFile(this.rootFile, path + separator + fileName, (err) => {
        console.log(err);
      });
    });
  }
}
